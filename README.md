# 使用Remix与metamask部署ERC20代币



## 1.搭建工作环境

安装remix-ide用来编译和部署智能合约，metamask则是一款浏览器上的以太坊轻钱包插件。

remix-ide有在线的版本，但其稳定性视网络情况而定，建议安装离线版的remix-ide。

安装好metamask后，将其左上角的网络设置为Ropsten测试网络。



## 2.编写合约

合约代码如下：

```js
pragma solidity ^0.4.8;
contract Token {
    /// token总量，默认会为public变量生成一个getter函数接口，名称为totalSupply().
    uint256 public totalSupply;

    /// 获取账户_owner拥有token的数量
    function balanceOf(address _owner) constant returns (uint256 balance);

    //从消息发送者账户中往_to账户转数量为_value的token
    function transfer(address _to, uint256 _value) returns (bool success);

    //从账户_from中往账户_to转数量为_value的token，与approve方法配合使用
    function transferFrom(address _from, address _to, uint256 _value) returns  (bool success);

    //消息发送账户设置账户_spender能从发送账户中转出数量为_value的token
    function approve(address _spender, uint256 _value) returns (bool success);

    //获取账户_spender可以从账户_owner中转出token的数量
    function allowance(address _owner, address _spender) constant returns  (uint256 remaining);

    //发生转账时必须要触发的事件 
    event Transfer(address indexed _from, address indexed _to, uint256 _value);

    //当函数approve(address _spender, uint256 _value)成功执行时必须触发的事件
    event Approval(address indexed _owner, address indexed _spender, uint256 _value);
}

contract StandardToken is Token {
    function transfer(address _to, uint256 _value) returns (bool success) {
        //默认totalSupply 不会超过最大值 (2^256 - 1).
        //如果随着时间的推移将会有新的token生成，则可以用下面这句避免溢出的异常
        //require(balances[msg.sender] >= _value && balances[_to] + _value >balances[_to]);
        require(balances[msg.sender] >= _value);
        balances[msg.sender] -= _value;//从消息发送者账户中减去token数量_value
        balances[_to] += _value;//往接收账户增加token数量_value
        Transfer(msg.sender, _to, _value);//触发转币交易事件
        return true;
    }
    function transferFrom(address _from, address _to, uint256 _value) returns (bool success) {
        //require(balances[_from] >= _value && allowed[_from][msg.sender] >= _value && balances[_to] + _value > balances[_to]);
        require(balances[_from] >= _value && allowed[_from][msg.sender] >=  _value);
        balances[_to] += _value;//接收账户增加token数量_value
        balances[_from] -= _value; //支出账户_from减去token数量_value
        allowed[_from][msg.sender] -= _value;//消息发送者可以从账户_from中转出的数量减少_value
        Transfer(_from, _to, _value);//触发转币交易事件
        return true;
    }
    //查询余额
    function balanceOf(address _owner) constant returns (uint256 balance) {
        return balances[_owner];
    }
    //授权账户_spender可以从消息发送者账户转出数量为_value的token
    function approve(address _spender, uint256 _value) returns (bool success)   
    {
        allowed[msg.sender][_spender] = _value;
        Approval(msg.sender, _spender, _value);
        return true;
    }
    function allowance(address _owner, address _spender) constant returns (uint256 remaining) {
      return allowed[_owner][_spender];//允许_spender从_owner中转出的token数
    }

    mapping (address => uint256) balances;
    mapping (address => mapping (address => uint256)) allowed;
}

contract YangToken is StandardToken {
    /* Public variables of the token */
    string public name;                   //名称: eg Simon Bucks
    uint8 public decimals;                //最多的小数位数How many decimals to show. ie. There could 1000 base units with 3 decimals. Meaning 0.980 SBX = 980 base units. It's like comparing 1 wei to 1 ether.
    string public symbol;                 //token简称: eg SBX
    string public version = 'H0.1';       //版本

    function YangToken(uint256 _initialAmount, string _tokenName, uint8 _decimalUnits, string _tokenSymbol) {
        balances[msg.sender] = 100000; // 初始token数量给予消息发送者
        totalSupply = 100000;         // 设置初始总量
        name = 'YangToken';             // token名称
        decimals = 0;               // 小数位数
        symbol = 'YANG';             // token简称
    }
    /* 同意转出并调用接收合约（根据自己需求实现） */
    function approveAndCall(address _spender, uint256 _value, bytes _extraData) returns (bool success) {
        allowed[msg.sender][_spender] = _value;
        Approval(msg.sender, _spender, _value);
        //call the receiveApproval function on the contract you want to be notified. This crafts the function signature manually so one doesn't have to include a contract in here just for this.
        //receiveApproval(address _from, uint256 _value, address _tokenContract, bytes _extraData)
        //it is assumed that when does this that the call *should* succeed, otherwise one would use vanilla approve instead.
        
        require(_spender.call(bytes4(bytes32(sha3("receiveApproval(address,uint256,address,bytes)"))), msg.sender, _value, this, _extraData));
        return true;
    }
}
```



## 3.编译并部署合约

将代码写到remix-ide中后，选择右边选项卡中的Compile栏，点击编译；编译成功之后选择Run栏，注意此时`Environment`选项中应该是`Injected Web3`，`Account`中应该是metamask中登录的账户地址。

![run_config](https://gitee.com/Arctan/deployment_of_erc20_token/blob/master/pic/run_config.png)

然后在下面选择所需要部署的合约（本例中为YangToken），点击`Deploy`进行部署，

![deploy](pic\deploy.png)

随后metamask会弹出提示，

![deploying](pic\deploying.png)

点击`SUBMIT提交`将该交易请求发布至Ropsten网络中，交易成功打包后 ，可以查询到:

本次交易id为`0x60c874b3cde0f860c9507ab21c85042ba48005c3c806a9438075bf674a734180`

合约的地址为`0x1e7a62d97C3e94F0a189eA9DFDAa254B793DEc34`	

交易的详情如图：

![trans](pic\trans.png)

合约的详情如图：

![contract](pic\contract.png)

## 4.添加代币到账户

部署合约之后，使用metamask轻钱包添加该代币到账户中，只需要在metamask中选择添加代币，然后输入该合约的地址，点击`添加`即可，添加成功后，可以看到账户中得到了100000个YANG代币，

![add_token](pic\add_token.png)


